<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head><style>
#seats {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#seats td, #seats th {
  border: 1px solid #ddd;
  padding: 8px;
}

#seats tr:nth-child(even){background-color: #f2f2f2;}

#seats tr:hover {background-color: #ddd;}

#seats th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
<title>Add Student</title></head>

<body bgcolor="PapayaWhip">

<form action="save" method="post">
<br><br>
<table id="seats" border="">
<tr>
<td><div align="center">Add Student Seats</div></td></tr>
<tr>
<td>
<label for="SeatNo">Seat No</label> &nbsp;&nbsp;&nbsp;
<input type="text" name="seatno"><br><br></td></tr>
<tr>
<td>
<label for="Name">Name</label> &nbsp;&nbsp;&nbsp;
<input type="text" name="name"><br><br></td></tr>

<tr>
<td>
<label for="Course">Course</label>&nbsp;&nbsp;&nbsp;
<!-- <input type="text" name="course"><br><br></td></tr> -->
<select name="course">
<option value="Select">Select</option>
<option value="BCA">BCA</option>
<option value="BSC Electronics">BSC Electronics</option>
<option value="BSc ComputerScience">Bsc ComputerScience</option>
<option value="BA History">BA History</option>
</select>
<tr>
<td>
<label for="Department">Department</label>&nbsp;&nbsp;&nbsp;
<!-- <input type="text" name="dep"><br><br></td></tr> -->
<select name="dep">
<option value="Select">Select</option>
<option value="Computer Science">Computer Science</option>
<option value="Electronics">Electronics</option>
<option value="History">History</option>
</select>
<tr>
<td>
<label for="Mobile">Mobile</label> &nbsp;&nbsp;&nbsp;
<input type="text" name="mobile"><br><br></td></tr>

<tr>
<td>
<label for="Address">Address</label> &nbsp;&nbsp;&nbsp;
<input type="text" name="address"><br><br></td></tr>

<tr>
<td><input type="submit" value="Add"></td></tr>


</table><br><br><br>
<a href="aback">Go to Previous Page</a>


</form>
<br><br>


</body>
</html>