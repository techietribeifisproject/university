<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%> 
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View All</title>
</head>
<%@ page isELIgnored="false" %>
<body style="background-color:AntiqueWhite">
<br><br><br>

	
			<table border="1" style="width: 100%">
				<tr>
					<th>Seat No</th>
					<th>Name</th>
					<th>Course</th>
					<th>Department</th>
					<th>Address</th>
				</tr>
				<%-- <c:if test="${not empty course}"> --%>
		<c:forEach var="n" items="${course}">
				<tr>
					<td><c:out value="${n.seatno}" /></td>
					<td><c:out value="${n.name}" /></td>
					<td><c:out value="${n.course}" /></td>
					<td><c:out value="${n.dep}" /></td>
					<td><c:out value="${n.address}" /></td>
				</tr>
</c:forEach>
	<%-- </c:if> --%>
			</table>
 <br><br><br>
<a href="cback">Go to previous page</a>
</body>
</html>