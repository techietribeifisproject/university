<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%-- <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> --%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%-- <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %> --%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<%@ page isELIgnored="false" %>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Student Page</title>
<style>
#seats {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#seats td, #seats th {
  border: 1px solid #ddd;
  padding: 8px;
}

#seats tr:nth-child(even){background-color: #f2f2f2;}

#seats tr:hover {background-color: #ddd;}

#seats th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body bgcolor="Azure">
<div align="center">
<h3>Student Details</h3>
	<table border="1" id="seats">
		<thead>
			<tr>
			<th>Seat No</th>
				<th>Name</th>
				<th>Course</th>
				<th>Department</th>
				<th>Mobile</th>
				<th>Address</th>
			</tr>
		</thead>
		<tbody>
		<%-- <c:if test="${not empty v}"> --%>
			<c:forEach var="e" items="${seat}">
				<tr>
				<td><c:out value="${e.seatno}" /></td>
					<td><c:out value="${e.name}" /></td>
					<td><c:out value="${e.course}" /></td>
					<td><c:out value="${e.dep}" /></td>
					<td><c:out value="${e.mobile}" /></td>
					<td><c:out value="${e.address}" /></td>
					
				</tr>
			</c:forEach>
		<%-- </c:if> --%>
		</tbody>
	</table>
</div>
<br><br>
<a href="eback">Go to previous page</a>
</body>
</html>