package com.asminds.SeatReg.Controller;




import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.asminds.SeatReg.model.persistence.StudentSeatReg;
import com.asminds.SeatReg.model.service.SeatRegService;


@Controller
public class SeatRegContoller {
	@Autowired
	SeatRegService ss;
	
	@RequestMapping(value={"/","homepage"})
	public String home(){
		System.out.println("I am in home page");
		return "index";
	}
	@RequestMapping("/adminlogin")
	public String alogin(){
		System.out.println("I am in admin login page");
		return "AdminLogin";
	}
	
	@RequestMapping("/alogin")
	public String alogin(HttpServletRequest req,HttpServletResponse res){
		String u=req.getParameter("user");
		String p=req.getParameter("pass");
		if(u.equals("admin")&&p.equals("admin@123"))
			return "Admin";
		else
			return "Errorpage";
	}

	@RequestMapping("/aback")
	public String aback(){
		return "Admin";
	}
	
	@RequestMapping("/addseats")
	public String addseats(){
		return "Addseats";
	}
	
	@RequestMapping("/save")
	public String save(@ModelAttribute("h") StudentSeatReg s) {
		ss.save(s);
		System.out.println("i am in save page");
		return "Addseats";
	}
	
	@RequestMapping("/editseats")
	public String editseats(){
		return "Editseats";
	}
	
	@RequestMapping("/eback")
	public String eback(){
		return "Editseats";
	}
	@RequestMapping(value="/seatedit", method=RequestMethod.POST)
	public ModelAndView edit(@ModelAttribute("l") StudentSeatReg e,ModelAndView mv) {
		System.out.println("I am in edit seat Page");
	/*mv.setViewName("ViewStudentSeats");
	mv.addObject("seat",ss.getseatById(e.getSeatno()));
	System.out.println(ss.getseatById(e.getSeatno()));*/
		//return mv;
		
	List<StudentSeatReg> li=ss.getseat(e);
	return new ModelAndView("ViewStudentSeats","seat",li);

	}
	
	/*@RequestMapping(value="/seatedit", method=RequestMethod.POST)
	public ModelAndView edit(@ModelAttribute("l") StudentSeatReg st,ModelAndView mv) {
		System.out.println("I am in edit seat Page");
		
		List<StudentSeatReg> li=ss.getseat();
		return new ModelAndView("ViewStudentSeats","seat",li);
	mv.setViewName("editbyid");
	mv.addObject("b",bookService.getBookById(b.getId()));
	System.out.println(bookService.getBookById(b.getId()));
	return mv;
	 
	}*/
	
	@RequestMapping("/editcourse")
	public String editcourse(){
		System.out.println("I am edit page");
		return "Editcourse";
	}
	
	@RequestMapping(value="/courseedit", method=RequestMethod.POST)
	public ModelAndView editcourse(@ModelAttribute("l") StudentSeatReg e,ModelAndView mv) {
		System.out.println("I am in edit course Page");
		List<StudentSeatReg> li=ss.getcourse(e);
		return new ModelAndView("ViewStudentCourse","course",li);
	
	 
	}
	
	@RequestMapping("/cback")
	public String editcourses(){
		return "Editcourse";
	}
	
}
