package com.asminds.SeatReg.model.repository;

import java.util.List;


import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.asminds.SeatReg.model.persistence.StudentSeatReg;
@Repository
@SuppressWarnings({ "unchecked", "deprecation" ,"rawtypes"})
public class SeatRegRepImpl implements SeatRegRepository{
	
	SessionFactory sf;
	@Autowired
	public SeatRegRepImpl(SessionFactory sf) {
		super();
		this.sf = sf;
	}
	
	public void save(StudentSeatReg s) {
		sf.getCurrentSession().save(s);
	}

	
	public List<StudentSeatReg> getseat(StudentSeatReg s) {
		
		Query q=sf.getCurrentSession().createQuery("from StudentSeatReg where seatno=:seatno");
		q.setParameter("seatno",s.getSeatno());
		List l=q.list();
		l.forEach(c->System.out.println(c));
		return l;
	}

	public StudentSeatReg getseatById(int seatno) {
		StudentSeatReg s=(StudentSeatReg)sf.getCurrentSession().get(StudentSeatReg.class,seatno);
		return s;
	}

	public StudentSeatReg getseatByCourse(String course) {
		StudentSeatReg st=(StudentSeatReg)sf.getCurrentSession().get(StudentSeatReg.class, course);
		return st;
	}


	public List<StudentSeatReg> getcourse(StudentSeatReg st) {
		Query q=sf.getCurrentSession().createQuery("from StudentSeatReg where course=:course");
		q.setParameter("course",st.getCourse());
		List l=q.list();
		l.forEach(c->System.out.println(c));
		return l;
	}
	
	
	
	

}
