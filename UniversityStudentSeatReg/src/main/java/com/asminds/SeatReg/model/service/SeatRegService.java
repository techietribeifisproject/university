package com.asminds.SeatReg.model.service;

import java.util.List;

import com.asminds.SeatReg.model.persistence.StudentSeatReg;

public interface SeatRegService {
public void save(StudentSeatReg s);
public List<StudentSeatReg> getseat(StudentSeatReg s);
public StudentSeatReg getseatById(int seatno);
public StudentSeatReg getseatByCourse(String course);
public List<StudentSeatReg> getcourse(StudentSeatReg st);
}
