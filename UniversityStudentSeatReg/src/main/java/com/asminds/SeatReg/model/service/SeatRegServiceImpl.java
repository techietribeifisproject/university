package com.asminds.SeatReg.model.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.asminds.SeatReg.model.persistence.StudentSeatReg;
import com.asminds.SeatReg.model.repository.SeatRegRepository;

@Service
@Transactional
public class SeatRegServiceImpl implements SeatRegService{

	SeatRegRepository rep;
	@Autowired
	public SeatRegServiceImpl(SeatRegRepository rep) {
		super();
		this.rep = rep;
	}
	
	public void save(StudentSeatReg s) {
		rep.save(s);
		
	}

	public List<StudentSeatReg> getseat(StudentSeatReg s) {
		
		return rep.getseat(s);
	}

	public StudentSeatReg getseatById(int seatno) {
		
		return rep.getseatById(seatno);
	}

	public StudentSeatReg getseatByCourse(String course) {
		
		return rep.getseatByCourse(course);
	}

	@Override
	public List<StudentSeatReg> getcourse(StudentSeatReg st) {
	
		return rep.getcourse(st);
	}

	
	
	
}
