package com.asminds.SeatReg.model.persistence;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class StudentSeatReg {
	@Id
	@GeneratedValue
	int id;
	int seatno;
	String name;
	String course,dep,address;
	Long mobile;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSeatno() {
		return seatno;
	}
	public void setSeatno(int seatno) {
		this.seatno = seatno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	public String getDep() {
		return dep;
	}
	public void setDep(String dep) {
		this.dep = dep;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Long getMobile() {
		return mobile;
	}
	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}
	public StudentSeatReg() {
		super();
		// TODO Auto-generated constructor stub
	}
	public StudentSeatReg(int id, int seatno, String name, String course, String dep, String address, Long mobile) {
		super();
		this.id = id;
		this.seatno = seatno;
		this.name = name;
		this.course = course;
		this.dep = dep;
		this.address = address;
		this.mobile = mobile;
	}
	@Override
	public String toString() {
		return "StudentSeatReg [id=" + id + ", seatno=" + seatno + ", name=" + name + ", course=" + course + ", dep="
				+ dep + ", address=" + address + ", mobile=" + mobile + "]";
	}
	

}
